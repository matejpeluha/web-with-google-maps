
function initMap() {
    const feiLocation = new google.maps.LatLng(48.151865, 17.073347);

    const map = createMap(feiLocation);
    createFeiMarker(map, feiLocation);

    initBusStopButton(map);
    initStreetView(map);
    initSearch(map, feiLocation);
}


function createMap(feiLocation){
    const mapProp = {
        center: feiLocation,
        zoom:17,
        streetViewControl: false
    };

    const map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

    const transitLayer = new google.maps.TransitLayer();
    transitLayer.setMap(map);

    return map;
}

function createFeiMarker(map, feiLocation){
    const image = {
        url: "resources/mapMarkers/feiMarker.svg",
        scaledSize: new google.maps.Size(70, 70)
    }

    const marker = new google.maps.Marker({
        position: feiLocation,
        icon: image,
        title: "STU FEI",
    });

    marker.setMap(map);
    setMarkerPopup(map, marker);
}

function setMarkerPopup(map, marker){
    const infowindow = new google.maps.InfoWindow({
        content: "<p>" + "Zemepisná šírka: 48.152877" + "</p>" + "Zemepisná dĺžka: 17.073205"
    });

    google.maps.event.addDomListener(marker, 'click', function() {
        infowindow.open(map, marker);
    });
}

function initBusStopButton(map){
    const busLocation = new google.maps.LatLng(48.154352, 17.074807);

    const button = document.getElementById("busStopsButton");
    button.addEventListener("click", () => {
        map.setCenter(busLocation);
        map.setZoom(18);
    });

    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(button);
}

function initStreetView(map){
    const panorama = new google.maps.StreetViewPanorama(
        document.getElementById("streetView"),
        {
            position: map.getCenter()
        }
    );
    map.setStreetView(panorama);
}

function initSearch(map, endLocation){
    let markers = [];
    let directions = []

    const input = document.getElementById("searchInput");
    const searchBox = new google.maps.places.SearchBox(input);

    searchBox.addListener("places_changed", () => {
        const places = searchBox.getPlaces();
        if (!places || places.length === 0) {
            return;
        }

        markers = clearMarkers(markers);
        directions = clearDirections(directions);
        showPlace(map, places, markers, directions, endLocation);
    });

    const radio1 = document.getElementById("driving");
    radio1.addEventListener("change", () =>{
        const places = searchBox.getPlaces();
        if (!places || places.length === 0) {
            return;
        }

        markers = clearMarkers(markers);
        directions = clearDirections(directions);
        showPlace(map, places, markers, directions, endLocation);
    })

    const radio2 = document.getElementById("walking");
    radio2.addEventListener("change", () =>{
        const places = searchBox.getPlaces();
        if (!places || places.length === 0) {
            return;
        }

        markers = clearMarkers(markers);
        directions = clearDirections(directions);
        showPlace(map, places, markers, directions, endLocation);
    })


}


function clearDirections(directions){
    directions.forEach((direction) => {
        direction.setMap(null);
    });
    return [];
}

function clearMarkers(markers){
    markers.forEach((marker) => {
        marker.setMap(null);
    });
    return [];
}

function showPlace(map, places, markers, directions, endLocation){
    places.forEach((place) => {
        if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
        }

        setNewMarker(map, place, markers);
        showDirection(map, directions, place.geometry.location, endLocation);
    });

}

function showDirection(map, directions, start, end){
    const directionsRenderer = new google.maps.DirectionsRenderer();
    directionsRenderer.setOptions({suppressMarkers: true});
    directionsRenderer.setMap(map)
    const directionsService = new google.maps.DirectionsService();

    const travelMode = document.querySelector('input[name="travelMode"]:checked').value;

    const request = {
        origin: start,
        destination: end,
        travelMode: travelMode,
        avoidHighways: false,
        avoidTolls: false

    };
    directionsService.route(request, function(result, status) {
        if (status === 'OK')
            directionsRenderer.setDirections(result);

        saveAttributes(result, status);
    });

    directions.push(directionsRenderer)

    setBounds(map, start, end);
}

function saveAttributes(result, status){
    const distance = document.getElementById("distance");
    const time = document.getElementById("time");

    distance.innerText = "Vzdialenosť: ";
    time.innerText = "Čas: ";

    if(status === 'OK'){
        let directionsData = result.routes[0].legs[0];
        distance.innerText += " " + directionsData.distance.text;
        time.innerText += " " + directionsData.duration.text;
    }
}

function setBounds(map, start, end){
    const bounds = new google.maps.LatLngBounds();
    bounds.extend(start);
    bounds.extend(end);
    map.fitBounds(bounds);
}

function setNewMarker(map, place, markers){
    markers.push(
        new google.maps.Marker({
            map,
            title: place.name,
            position: place.geometry.location,
        })
    );
}
